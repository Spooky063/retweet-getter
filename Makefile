PIP := $(shell command -v pip3 2> /dev/null)

default: lib
all: prerequisites

prerequisites:
ifndef PIP
    $(error "pip3 is not available please install pip3")
endif

PHONY: lib
lib: prerequisites
	pip3 install tweepy

PHONY: tweet
tweet:
	python3 twitter.py -t $(filter-out $@,$(MAKECMDGOALS))