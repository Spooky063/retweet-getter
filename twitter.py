import json
import calendar
import time
import sys
import getopt

import tweepy

class Twitter():

    CONSUMER_KEY=""
    CONSUMER_SECRET=""
    ACCESS_TOKEN=""
    ACCESS_TOKEN_SECRET=""

    api = None

    def __init__(self):
        auth = tweepy.OAuthHandler(self.CONSUMER_KEY, self.CONSUMER_SECRET)
        auth.set_access_token(self.ACCESS_TOKEN, self.ACCESS_TOKEN_SECRET)
        api = tweepy.API(auth)
        self.api = api


    def retweet(self, argv):
        tweetId = ""

        try:
            options, args = getopt.getopt(argv, "t:", ["tweetId ="])
        except:
            print("python3 twitter.py 1409912324408975362")

        for name, value in options:
            if name in ['-t', '--tweetId']:
                tweetId = value

        count = 10000
        retweet_list = self.api.retweets(tweetId, count)

        data = []
        for retweet in retweet_list:
            element = {}
            element['name'] = retweet.user.name
            element['username'] = retweet.user.screen_name
            element['url'] = "https://twitter.com/" + retweet.user.screen_name
            element['created_at'] = retweet.user.created_at.strftime('%d-%m-%Y, %H:%M:%S')
            data.append(element)

        gmt = time.gmtime()
        with open('retweet_' + tweetId + '_' + str(calendar.timegm(gmt)) + '.json', 'w') as json_file:
            json.dump(data, json_file, indent=4)

        print("Use http://convertcsv.com/json-to-csv.htm to convert json data to csv")

if __name__ == '__main__':
    if len(sys.argv[1:]) == 0:
        print("No tweetID args passed to function")
        sys.exit(2)
    else:
        api = Twitter()
        api.retweet(sys.argv[1:])
