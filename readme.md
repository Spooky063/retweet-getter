# Twitter polls

## About

Retrieve all people RT specific tweet.

## Install

Installation of libraries to run the script

```
make lib
```

Run script for specific tweet

```
make tweet TWEETID
```